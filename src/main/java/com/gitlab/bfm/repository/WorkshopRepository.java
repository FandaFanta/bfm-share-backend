package com.gitlab.bfm.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gitlab.bfm.entity.Workshop;

public interface WorkshopRepository extends MongoRepository<Workshop, Long> {

}
