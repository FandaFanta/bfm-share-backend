package com.gitlab.bfm.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gitlab.bfm.entity.Offer;

public interface OffersRepository extends MongoRepository<Offer, Long> {

}
