package com.gitlab.bfm.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gitlab.bfm.entity.UserProfile;

public interface UserProfileRepository extends MongoRepository<UserProfile, Long> {

	UserProfile findByUsername(String username);

}
