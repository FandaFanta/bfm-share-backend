package com.gitlab.bfm.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gitlab.bfm.entity.Request;

public interface RequestsRepository extends MongoRepository<Request, Long> {

}
