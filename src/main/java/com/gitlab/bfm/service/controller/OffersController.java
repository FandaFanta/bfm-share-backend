package com.gitlab.bfm.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.bfm.entity.Offer;
import com.gitlab.bfm.repository.OffersRepository;

@CrossOrigin
@RestController
public class OffersController {
	
	@Autowired
	private OffersRepository offerspeository;
	
	@ResponseBody
	@GetMapping(value = "offers/all")
    public List<Offer> getAllOffers() {
        return this.offerspeository.findAll();
    }
}
