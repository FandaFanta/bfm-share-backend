package com.gitlab.bfm.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.bfm.entity.Workshop;
import com.gitlab.bfm.repository.WorkshopRepository;

@CrossOrigin
@RestController
public class WorkshopController {
	
	@Autowired
	private WorkshopRepository workshopRepository;
	
	@ResponseBody
	@GetMapping(value = "workshops/all")
    public List<Workshop> getAllWorkshops() {
        return this.workshopRepository.findAll();
    }
}
