package com.gitlab.bfm.service.controller;

import javax.websocket.server.PathParam;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.gitlab.bfm.entity.UserProfile;
import com.gitlab.bfm.repository.UserProfileRepository;

/**
 * Created on 20.10.18.
 *
 * @author bvfnbk
 */
@CrossOrigin
@Slf4j
@RestController
public class UserProfileController {
	
	@Autowired
	private UserProfileRepository userProfileRepository;
	
	@ResponseBody
	@RequestMapping(
            method = RequestMethod.GET,
			path = "/user/profile/{username}",
			produces = MediaType.APPLICATION_JSON_VALUE
	)
    public UserProfile getUserProfile(@PathVariable String username) {
	    log.info("getUserProfile({})", username);
        return this.userProfileRepository.findByUsername(username);
    }
}
