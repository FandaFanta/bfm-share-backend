package com.gitlab.bfm.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.bfm.entity.Request;
import com.gitlab.bfm.repository.RequestsRepository;

@CrossOrigin
@RestController
public class RequestsController {
	
	@Autowired
	private RequestsRepository requestspeository;
	
	@ResponseBody
	@GetMapping(value = "requests/all")
    public List<Request> getAllRequests() {
        return this.requestspeository.findAll();
    }
}
