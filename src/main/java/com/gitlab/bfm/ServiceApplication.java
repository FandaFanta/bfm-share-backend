package com.gitlab.bfm;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gitlab.bfm.entity.Offer;
import com.gitlab.bfm.entity.Request;
import com.gitlab.bfm.entity.UserProfile;
import com.gitlab.bfm.entity.Workshop;
import com.gitlab.bfm.repository.OffersRepository;
import com.gitlab.bfm.repository.RequestsRepository;
import com.gitlab.bfm.repository.UserProfileRepository;
import com.gitlab.bfm.repository.WorkshopRepository;

@SpringBootApplication
public class ServiceApplication implements CommandLineRunner {

    private final UserProfileRepository userProfileRepository;
    
    private final WorkshopRepository workshopRepository;
    
    private final RequestsRepository requestsRepository;
    
    private final OffersRepository offersRepository;

    @Autowired
    public ServiceApplication(UserProfileRepository userProfileRepository, WorkshopRepository workshopRepository, RequestsRepository requestsRepository, OffersRepository offersRepository) {
        this.userProfileRepository = userProfileRepository;
        this.workshopRepository = workshopRepository;
        this.requestsRepository = requestsRepository;
        this.offersRepository = offersRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(ServiceApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        userProfileRepository.deleteAll();

        List<Workshop> workshops = new ArrayList<>();
        Workshop workshop1 = new Workshop();
        workshop1.setName("First Steps Docker");
        workshop1.setDate(LocalDate.now());
        workshops.add(workshop1);
        Workshop workshop2 = new Workshop();
        workshop2.setName("Java Programming");
        workshop2.setDate(LocalDate.now());
        workshops.add(workshop2);
        Workshop workshop3 = new Workshop();
        workshop3.setName("Qality Management");
        workshop3.setDate(LocalDate.now());
        workshops.add(workshop3);
        Workshop workshop4 = new Workshop();
        workshop4.setName("Table Tennis Basics");
        workshop4.setDate(LocalDate.now());
        workshops.add(workshop4);
        Workshop workshop5 = new Workshop();
        workshop5.setName("Learning Chess");
        workshop5.setDate(LocalDate.now());
        workshops.add(workshop5);
        Workshop workshop6 = new Workshop();
        workshop6.setName("Basketball Turnier");
        workshop6.setDate(LocalDate.now());
        workshops.add(workshop6);
        
        List<Request> requests = new ArrayList<>();
        Request request1 = new Request();
        request1.setTitle1("Qualitästssicherung im Projekt");
        request1.setTitle2("Test-Framework gesucht");
        request1.setDescription("Wir haben Probleme in der Qualitätssicherung. Wir suchen ein Test-Framework für Integrationsstests. Wer Erfahrung in dem Bereiche hate bitte bei mir melden.");
        requests.add(request1);
        Request request2 = new Request();
        request2.setTitle1("Suche Mitfahrgelegenheit");
        request2.setTitle2("Trudering - München (IDV)");
        request2.setDescription("Nur Mittwoch und Donnerstag, bin zeitlich flexibel.");
        requests.add(request2);
        Request request3 = new Request();
        request3.setTitle1("Suche Winterreifen");
        request3.setTitle2("Für Golf 5 - 15 oder 16 Zoll");
        request3.setDescription("");
        requests.add(request3);
        Request request4 = new Request();
        request4.setTitle1("Suche Wohnung");
        request4.setTitle2("3 oder 3,5 Zimmer, maximal 1300 Euro warm");
        request4.setDescription("");
        requests.add(request4);
        Request request5 = new Request();
        request5.setTitle1("Suche Lauf-Partner");
        request5.setTitle2("Nach Feierabned oder in der Mittagspause");
        request5.setDescription("");
        requests.add(request5);
        
        
        List<Offer> offers = new ArrayList<>();
        Offer offer1 = new Request();
        offer1.setTitle1("Biete Mitfahrgelegenheit zur IDV");
        offer1.setTitle2("Starnberg - München und zurück");
        offer1.setDescription("Montag bis Freitag - gegene eine geringe Gebühr :-) ");
        offers.add(offer1);
        Offer offer2 = new Request();
        offer2.setTitle1("Verkaufe Winterreifen");
        offer2.setTitle2("16 Zoll, 3 Jahre alt");
        offer2.setDescription("");
        offers.add(offer2);
        Offer offer3 = new Request();
        offer3.setTitle1("Biete Unterstützung im Thema Qualitätssicherung");
        offer3.setTitle2("Insbes. Test-Frameworks in Java");
        offer3.setDescription("");
        offers.add(offer3);
        Offer offer4 = new Request();
        offer4.setTitle1("Biete Kochkurs in der Mittagspause");
        offer4.setTitle2("Griechisch, mit viel Fleisch");
        offer4.setDescription("warm oder kalt");
        offers.add(offer4);
        Offer offer5 = new Request();
        offer5.setTitle1("Schnuppertennis nach Feierabend");
        offer5.setTitle2("Natürlich mit Bierchen danach");
        offer5.setDescription("Montag oder Donnerstag");
        offers.add(offer5);
        
        UserProfile newProfile = new UserProfile();
        newProfile.setUsername("testuser");
        newProfile.setFirstName("Rainer");
        newProfile.setLastName("Zufall");
//        newProfile.setSkills(skills);
        newProfile.setOffers(offers);
        newProfile.setRequests(requests);
        newProfile.setWorkshops(workshops);
        
        offersRepository.saveAll(offers);
        requestsRepository.saveAll(requests);
        workshopRepository.saveAll(workshops);
        userProfileRepository.save(newProfile);
    }
}
