package com.gitlab.bfm.entity;

import java.util.List;

import lombok.Data;

@Data
public class Request extends Offer {

	private List<Skill> requiredSkills;

}
