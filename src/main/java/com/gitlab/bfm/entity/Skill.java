package com.gitlab.bfm.entity;

import java.util.List;

import lombok.Data;

@Data
public class Skill extends AbstractEntitiy {

	private String name;
	
	private List<Tag> tags;
	
}
