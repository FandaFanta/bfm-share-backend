package com.gitlab.bfm.entity;

import java.time.LocalDate;

import lombok.Data;

@Data
public class Workshop extends AbstractEntitiy {

	private String name;
	
	private LocalDate date;
	
	private String imageUrl;
	
}
