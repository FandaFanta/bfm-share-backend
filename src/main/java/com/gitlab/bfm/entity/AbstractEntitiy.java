package com.gitlab.bfm.entity;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class AbstractEntitiy {

	@Id
	private String id;
	
}
