package com.gitlab.bfm.entity;

import lombok.Data;

@Data
public class Tag extends AbstractEntitiy {

	private String name;
	
}
