package com.gitlab.bfm.entity;

import java.util.List;

import lombok.Data;

@Data
public class UserProfile extends AbstractEntitiy {

	private String username;
	
	private String firstName;
	
	private String lastName;
	
	private List<Skill> skills;
	
	private List<Offer> offers;
	
	private List<Request> requests;
	
	private List<Workshop> workshops;
	
	private String imgLink;
	
}
