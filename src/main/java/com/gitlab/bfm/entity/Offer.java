package com.gitlab.bfm.entity;

import lombok.Data;

@Data
public class Offer extends AbstractEntitiy {
	
	private String description;

	private String title1;

	private String title2;
	
	private String title3;

	private String imgLink;
	
}
