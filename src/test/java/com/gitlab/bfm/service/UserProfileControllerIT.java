package com.gitlab.bfm.service;

import com.gitlab.bfm.entity.UserProfile;
import io.restassured.RestAssured;
import org.junit.Test;

/**
 * Created on 20.10.18.
 *
 * @author bvfnbk
 */
public class UserProfileControllerIT {

    @Test
    public void testGetUserProfile() {
        UserProfile userProfile = RestAssured
                .given().pathParam("username", "testuser")
                .when().log().all().get("http://localhost:8080/user/profile/{username}")
                .then().log().all().statusCode(200).extract().body().as(UserProfile.class);
    }
}
