# BFM Share (Backend)

## Build Docker Image

The image is implicitly built during the `package` phase, i.e.

```bash
mvn package
```

but can _explicit_ be built with

```bash
mvn docker:build
```

## Run

```bash
docker-compose up
```

## Build & Run

```bash
mvn package docker-compose:up
```